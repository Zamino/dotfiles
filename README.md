# My Dotfiles

I manage them with `git` and `stow`. `stow` creates symlinks to the files in
this folder. Just run inside **this** folder:

    stow -t ~ */
