#!/bin/bash
echo "
#############################################
# This script installes and sets up awesome #
#############################################
"

# exit when any command fails
set -e

if ! command -v awesome &> /dev/null; then
  echo "awesome is not installed"
  sudo pacman -S awesome
else
    echo "awesome is installed"
fi

config_dir="$HOME/.config/awesome"
if [ ! -d "$config_dir" ]; then
  echo "create config folder"
  mkdir -p "$config_dir"
fi

if [ ! -d "$config_dir/awesome-wm-widgets" ]; then
  echo "installing widgets"
  if ! command -v git &> /dev/null; then
    echo "git is not installed"
    sudo pacman -S git
  fi
  git clone https://github.com/streetturtle/awesome-wm-widgets "$config_dir/awesome-wm-widgets"
fi
