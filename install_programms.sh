#/bin/bash

yay -S \
alacritty \
connman-gtk \
gnome-system-monitor \
keepassxc \
mpd \
nextcloud-client \
nwg-launchers \
pavucontrol \
signal-desktop \
telegram-desktop \
thunderbird \
timeshift \
vifm \
vimpc \
wlogout \
wofi \
xsensors \

sudo gpg --recv-key 416F061063FEE659
yay -S vidalia
