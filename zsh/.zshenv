#Environment file. Runs on login. Environmental variables are set here.

## Default programs:
#export EDITOR="nvim"
#export PAGER="less"
#export TERMINAL="xterm"
#export BROWSER="firfox"
#export READER="xreader"

# ~/ Clean-up {{{
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_HOME="$HOME/.local/share"

export LESSHISTFILE="-"

export ZDOTDIR="$HOME/.config/zsh/"

#export GIT_DIR="$HOME/.config/git/"

export GTK2_RC_FIlES="$HOME/.config/gtk-2.0/gtkrc-2.0"

export PSQLRC="$HOME/.config/psql/.psqlrc"
export PSQL_HISTORY="$HOME/.config/psql/.psql_history"
export PSQL_PAGER='nvim -R -c "setlocal nolist colorcolumn=0 ft=sql"'
export PSQL_EDITOR='nvim -c "set ft=sql"'
export PSQL_EDITOR_LINENUMBER_ARG='+'

# }}}
