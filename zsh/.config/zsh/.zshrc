# Set $PATH if ~/.local/bin exist
if [ -d "$HOME/.local/bin" ]; then
    export PATH=$HOME/.local/bin:$PATH
fi

## Options section
setopt nocorrect                                                  # Auto correct mistakes
setopt extendedglob                                             # Extended globbing. Allows using regular expressions with *
setopt nocaseglob                                               # Case insensitive globbing
setopt rcexpandparam                                            # Array expension with parameters
setopt checkjobs                                              # Don't warn about running processes when exiting
setopt numericglobsort                                          # Sort filenames numerically when it makes sense
setopt nobeep                                                   # No beep
setopt autocd                                                   # if only directory path is entered, cd there.
setopt auto_pushd
setopt pushd_ignore_dups
setopt pushdminus

# histfile settings
HISTFILE=~/.config/zsh/.histfile
HISTSIZE=14000
SAVEHIST=12000
setopt appendhistory
setopt histexpiredupsfirst

unsetopt nomatch notify
# use vi keybindings
bindkey -v

## Promt anpassen
# https://wiki.archlinux.org/index.php/Zsh
RPS1='%B%F{46}%~%f%b'
# https://unix.stackexchange.com/a/1029
terminfo_down_sc=$terminfo[cud1]$terminfo[cuu1]$terminfo[sc]$terminfo[cud1]
vim_ins_mode="[INS]"
vim_cmd_mode="[CMD]"
function zle-line-init zle-keymap-select {
    #RPS1="${${KEYMAP/vicmd/-- NORMAL --}/(main|viins)/-- INSERT --}"
    vim_mode="${${KEYMAP/vicmd/$vim_cmd_mode}/(main|viins)/$vim_ins_mode}"
    PS1_2="$vim_mode"
    if [ "$USER" = "root" ]; then
      PS1="%{$terminfo_down_sc$PS1_2$terminfo[rc]%}%B%F{3}%n%F{1}@%F{3}%m%f%F{1}%#%f->%b"
    else
      PS1="%{$terminfo_down_sc$PS1_2$terminfo[rc]%}%B%F{34}%n%F{1}@%F{34}%m%f%F{1}%#%f->%b"
    fi
    zle reset-prompt
}
preexec () { print -rn -- $terminfo[el]; }
zle -N zle-line-init
zle -N zle-keymap-select

# Make Vi mode transitions faster (KEYTIMEOUT is in hundredths of a second)
export KEYTIMEOUT=1

# Edit line in vim with ctrl-e:
autoload edit-command-line; zle -N edit-command-line
bindkey '^e' edit-command-line

# fix backspace
bindkey -v '^?' backward-delete-char
# map ^h to backspace
bindkey -v '^h' backward-delete-char
# map ^w to delete a word
bindkey -v '^w' backward-delete-word

# asdf - Versionsverwaltung (elixir aber auch andere) {{{
if [ ! -d $HOME/.asdf ]; then
  git clone https://github.com/asdf-vm/asdf.git ~/.asdf --branch v0.13.1
fi
. $HOME/.asdf/asdf.sh
fpath=($HOME/.asdf/completions $fpath)
# }}}



# Basic auto/tab complete:
zstyle ':completion:*' menu select
zmodload zsh/complist
_comp_options+=(globdots) # Include hidden files.


# Use vim keys in tab complete menu:
bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-forward-char
bindkey -M menuselect 'j' vi-down-line-or-history


# <C-s> and <C-q> set free
stty -ixon

# history search in vim mode
# http://zshwiki.org./home/zle/bindkeys#why_isn_t_control-r_working_anymore
# ctrl+r to search history
bindkey -M viins '^s' history-incremental-search-backward
bindkey -M vicmd '^s' history-incremental-search-backward

# set pager to vim
# https://vim.fandom.com/wiki/Using_vim_as_a_man-page_viewer_under_Unix
# export COLUMNS=$COLUMNS-3    befor man
#export PAGER="/bin/sh -c \"unset PAGER; col -b -x | \
#  vim -R -c 'set ft=man nomod nolist' -c 'map q :q<CR>' \
#  -c 'map <SPACE> <C-D>' -c 'map b <C-U>' -\""
#export PAGER="nvim + c \'set ft=man\'"

#alias settings
setopt COMPLETE_ALIASES
source ~/.config/zsh/.aliasrc

# Gibt mittels Cowsay und Fortunes Sprueche aus
if [ -f /usr/games/fortune -a -d /usr/share/cowsay ]; then
  dir='/usr/share/cowsay/cows/'
  file=`/bin/ls -1 "$dir" | sort --random-sort | head -1`
  cow=$(echo "$file" | sed -e "s/\.cow//")
  /usr/games/fortune quiz wusstensie | /usr/games/cowsay -f $cow
fi

# Gibt Systeminfo aus
#neofetch

# HSTR configuration - add this to ~/.zshrc
# https://dvorka.github.io/hstr/CONFIGURATION.html
export HISTFILE=~/.config/zsh/.histfile  # ensure history file visibility
export HSTR_CONFIG=hicolor        # get more colors
bindkey -s "\C-r" "\eqhstr\n"     # bind hstr to Ctrl-r (for Vi mode check doc)

zstyle :compinstall filename '~/.config/zsh/.zshrc'

# Completion.
autoload -Uz compinit
compinit
zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}'       # Case insensitive tab completion
zstyle ':completion:*' rehash true                              # automatically find new executables in path
zstyle ':completion:*' list-colors "${(s.:.)LS_COLORS}"         # Colored completion (different colors for dirs/files/etc)
zstyle ':completion:*' completer _expand _complete _ignored _approximate
zstyle ':completion:*' menu select=2
zstyle ':completion:*' select-prompt '%SScrolling active: current selection at %p%s'
zstyle ':completion:*:descriptions' format '%U%F{cyan}%d%f%u'

# Speed up completions
zstyle ':completion:*' accept-exact '*(N)'
zstyle ':completion:*' use-cache on
zstyle ':completion:*' cache-path ~/.cache/zcache

# automatically load bash completion functions
autoload -U +X bashcompinit && bashcompinit


## Plugins section: Enable fish style features
# Use syntax highlighting
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
##https://github.com/zsh-users/zsh-syntax-highlighting
#[[ -e ~/.config/zsh/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh ]] && source ~/.config/zsh/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

# Use autosuggestion
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh

# Use history substring search
source /usr/share/zsh/plugins/zsh-history-substring-search/zsh-history-substring-search.zsh

# Search for pkgfile if command is not found
source /usr/share/doc/pkgfile/command-not-found.zsh

# Use fzf (after vi mode)
source /usr/share/fzf/key-bindings.zsh
source /usr/share/fzf/completion.zsh

# Arch Linux command-not-found support, you must have package pkgfile installed
# https://wiki.archlinux.org/index.php/Pkgfile#.22Command_not_found.22_hook
[[ -e /usr/share/doc/pkgfile/command-not-found.zsh ]] && source /usr/share/doc/pkgfile/command-not-found.zsh
