#!/bin/bash
echo "
##########################################
# This script installes and sets up nvim #
##########################################
"

# exit when any command fails
set -e

config_dir="$HOME/.config/nvim"

yay -S neovim-nightly-bin

if ! command -v git &> /dev/null; then
  echo "git is not installed"
  sudo pacman -S git
fi

if [ ! -d "$HOME/git/nvim" ]; then
  echo "downloading config from git"
  git clone https://gitlab.com/Zamino/nvim.git "$HOME/git/nvim"
fi

if [ ! -L "$config_dir" ]; then
  echo "linking config to $config_dir"
  ln -s "$HOME/git/nvim" "$config_dir"
fi
