#!/bin/bash
##########################################
# This script creates symlinks with stow #
##########################################

# --dotfiles will be added in 2.3.3

function create_folders () {
  # I don't wont all files in config folders to be in my git!
  echo "CREATING FOLDERS ===================================="
  folders="$(find ./*/ -maxdepth 2 -mindepth 1 -type d | sed -E 's/^\.\/[^/]*\///')"
  for folder in $folders ;do
    # replace `dot-` with `.`
    folder=$(echo "$folder" | sed -e 's/dot-/./g')
    if [ ! -e $HOME/$folder ]; then
      echo "creating folder $folder"
      mkdir -p $HOME/$folder
    fi
  done
}

# exit when any command fails
set -e

if ! command -v stow &> /dev/null; then
  echo "stow is not installed"
  exit
fi

case "$1" in
  "--test")
    echo "stow -vn -t ~ */"
    stow -vn -t ~ */
    ;;

  "--link")
    create_folders
    echo "CREATING SYMLINKS ==================================="
    stow -vS -t ~ */
    ;;

  "--link_way")
    create_folders
    echo "CREATING SYMLINKS FOR WAYLAND ======================="
    stow -vS -t ~ */
    stow -vD -t ~ xorg/
    ;;

  "--link_xorg")
    create_folders
    echo "CREATING SYMLINKS FOR XOR ==========================="
    stow -vS -t ~ */
    stow -vD -t ~ wayland/
    ;;

  "--reload")
    create_folders
    echo "RELOADING SYMLINKS ==================================="
    stow -vR -t ~ */
    ;;

  "--reload_way")
    create_folders
    echo "RELOADING SYMLINKS ==================================="
    stow -vR -t ~ */
    stow -vD -t ~ xorg/
    ;;

  "--reload_xorg")
    create_folders
    echo "RELOADING SYMLINKS ==================================="
    stow -vR -t ~ */
    stow -vD -t ~ wayland/
    ;;

  "--unlink")
    echo "REMOVING SYMLINKS ==================================="
    stow -vD -t ~ */
    ;;
  *)

    echo "Usage:
    --test        : show what symlinks would be created
    --link        : create all symlinks
    --link_way    : create symlinks for wayland
    --link_xorg   : create symlinks for xorg
    --reload_way  : removes and create symlinks
    --reload_xorg : removes and create symlinks for wayland
    --reload      : removes and create symlinks for xorg
    --unlink      : removes symlinks
    "
    ;;
esac
