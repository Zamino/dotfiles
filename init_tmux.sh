#!/bin/bash
echo "
##########################################
# This script installes and sets up tmux #
##########################################
"

# exit when any command fails
set -e

if ! command -v tmux &> /dev/null; then
  echo "tmux is not installed"
  sudo pacman -S tmux
else
    echo "tmux is installed"
fi

config_dir="$HOME/.config/tmux/plugins"
if [ ! -d "$config_dir" ]; then
  echo "create config folder"
  mkdir -p "$config_dir"
fi

if ! command -v git &> /dev/null; then
  echo "git is not installed"
  sudo pacman -S git
fi

echo "installing plugins"
if [ ! -d "$config_dir/tpm" ]; then
  git clone https://github.com/tmux-plugins/tpm "$config_dir/tpm"
fi
if [ ! -d "$config_dir/tmux-resurrect" ]; then
  git clone https://github.com/tmux-plugins/tmux-resurrect "$config_dir/tmux-resurrect"
fi
