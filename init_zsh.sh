#!/bin/bash
echo "
#########################################
# This script installes and sets up zsh #
#########################################
"

# exit when any command fails
set -e

if ! command -v zsh &> /dev/null; then
  echo "zsh is not installed"
  sudo pacman -S zsh
else
  echo "zsh is installed"
fi

shell="$(which zsh | sed -E 's/.*\///')"
if [ ! $shell = "zsh" ]; then
  echo "set defauld shell to zsh"
  path_to_zsh="$(which zsh)"
  user="$USER"
  chsh --shell $path_to_zsh $user
fi

config_dir="$HOME/.config/zsh"
if [ ! -d "$config_dir" ]; then
  echo "create config folder"
  mkdir -p "$config_dir"
fi

echo "installing plugins for zsh"
sudo pacman -S zsh-autosuggestions zsh-history-substring-search zsh-syntax-highlighting fzf pkgfile
