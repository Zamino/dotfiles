h
echo "
#######################################################################
# This script installes and makes additional setup for used programms #
#######################################################################
"

# exit when any command fails
set -e
if ! command -v yay &> /dev/null; then
  echo "yay is not installed"
  sudo pacman -S yay
fi

if ! command -v git &> /dev/null; then
  echo "git is not installed"
  sudo pacman -S git
fi

echo "Init zsh"
./init_zsh.sh

echo "Init awesome"
./init_awesome.sh

echo "Init tmux"
./init_tmux.sh

echo "Init neovim"
./init_neovim.sh

echo "
############
# FINISHED #
############
"
