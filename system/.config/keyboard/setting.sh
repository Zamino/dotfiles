#!/bin/bash

# map CapsLock to Esc
#xmodmap ~/.Xmodmap
#or
setxkbmap -option caps:escape

# ----------- END -------------

# set default keyboardlayout
setxkbmap gb;

# if Razer Keyboard is used change it to de
usbkbd_id=`xinput -list | grep "Razer DeathStalker  " | awk -F'=' '{print $2}' | cut -c 1-2 | tail -n 1`
usbkbd_layout="de"
if [ "${usbkbd_id}" ]; then
  setxkbmap -device "${usbkbd_id}" "${usbkbd_layout}"
fi

# if Ergodox Keyboard is used change it to gb
usbkbd_id=`xinput -list | grep "ZSA Ergodox EZ  " | awk -F'=' '{print $2}' | cut -c 1-2 | head -n 1`
usbkbd_layout="en_gb"
if [ "${usbkbd_id}" ]; then
  setxkbmap -device "${usbkbd_id}" "${usbkbd_layout}"
fi
